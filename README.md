# Go2NextFeaturePlus QGIS Plugin

This is an expertimental plugin to validate and integrate a local dataset to OpenStreetMap using QGIS. 

It's a fork of the ["Go2NextFeature QGIS plugin"](https://gitlab.com/albertodeluca/go2nextfeature) written by Alberto de Luca, which is still usable and functional without the enhancements.

For more information about this plugin see this white paper on ["Validating and Integrating Local Datasets in OpenStreetMap using QGIS"](https://md.coredump.ch/s/rk9r2k69Q).


# How to install

* Download or open the folder `Go2NextFeaturePlus` as a zip-file
* Extract the folder, check if's called `Go2NextFeaturePlus` and put it in `YOUR_PATH/QGIS/QGIS3/profiles/YOUR_PROFILE/python/plugins`  
e.g. `C:\Users\username\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins`
* Open or restart QGIS 3.X.X
* Go to 'Plugins' -> 'Manage and Install Plugins...'
* Enable the plugin
