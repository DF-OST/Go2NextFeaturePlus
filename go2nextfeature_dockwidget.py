# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Go2NextFeatureDockWidget
                                 A QGIS plugin
 Allows jumping from a feature to another following an attribute order
                             -------------------
        begin                : 2016-12-27
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Alberto De Luca for Tabacco Editrice
        email                : info@tabaccoeditrice.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import webbrowser
import os

from collections import OrderedDict
from enum import Enum
from .helper import first_occurrence_index

from qgis.core import *
from qgis.PyQt import QtGui, uic
from qgis.PyQt.QtCore import pyqtSignal, Qt
from qgis.PyQt.QtWidgets import (
    QVBoxLayout,
    QGridLayout,
    QFrame,
    QFormLayout,
    QLabel,
    QLineEdit,
    QComboBox,
    QHBoxLayout,
    QRadioButton,
    QButtonGroup,
    QCheckBox,
    QPushButton,
    QDockWidget,
    QSizePolicy,
)


class ButtonType(Enum):
    NEXT = 1
    PREVIOUS = 2
    FIRST = 3
    LAST = 4


from qgis.PyQt.Qt import QApplication
import operator

FORM_CLASS, _ = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "go2nextfeature_dockwidget.ui")
)


class Go2NextFeatureDockWidget(QDockWidget, FORM_CLASS):
    closingPlugin = pyqtSignal()
    plugin_name = "Go2NextFeature3+"

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(Go2NextFeatureDockWidget, self).__init__(parent)
        self.setupUi(self)
        self.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        self.iface = iface

        self.feats_list = []
        self.layer_id = None
        self.layer = None
        self.attribs_dict = OrderedDict()
        self.attrib = None
        self.feat_index = None
        self.selected_feat_id = None
        self.selected_feat_index = None
        self.selected_features = None

        self.tool = None

        self.changed_state_with_show = False

        self.fra_main.setLineWidth(3)
        fra_main_layer = QVBoxLayout(self.fra_main)
        fra_main_layer.setContentsMargins(0, 0, 0, 0)
        fra_main_layer.setSpacing(0)

        # TODO: Construct GUI layout in 'go2nextfeature_dockwidget.ui' file? See how its made in OSMDataSync
        # frame 1
        self.fra_1 = QFrame(self.fra_main)
        self.fra_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        fra_1_layer = QFormLayout(self.fra_1)

        self.lbl_layer = QLabel("Layer:")
        self.cbo_layer = QComboBox()
        self.cbo_layer.setMinimumContentsLength(10)
        self.cbo_layer.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_1_layer.addRow(self.lbl_layer, self.cbo_layer)

        self.lbl_attribute = QLabel("Order by:")
        self.cbo_order_by = QComboBox()
        self.cbo_order_by.setMinimumContentsLength(10)
        self.cbo_order_by.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_1_layer.addRow(self.lbl_attribute, self.cbo_order_by)

        self.chk_state = QCheckBox("Update state")

        self.lbl_state = QLabel("State Field:")
        self.cbo_state = QComboBox()
        self.cbo_state.setMinimumContentsLength(10)
        self.cbo_state.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        fra_1_layer.addRow(self.chk_state, self.cbo_state)

        self.lbl_action = QLabel("For next feature:")
        self.fra_action = QFrame(self.fra_1)
        fra_action_layer = QHBoxLayout(self.fra_action)
        fra_action_layer.setContentsMargins(5, 0, 5, 0)
        self.rad_action_pan = QRadioButton("pan")
        self.rad_action_pan.setChecked(True)
        self.rad_action_zoom = QRadioButton("pan and zoom")

        self.group_rad_action = QButtonGroup(self.fra_1)
        self.group_rad_action.addButton(self.rad_action_pan)
        self.group_rad_action.addButton(self.rad_action_zoom)

        fra_action_layer.addWidget(self.rad_action_pan)
        fra_action_layer.addWidget(self.rad_action_zoom)
        fra_1_layer.addRow(self.lbl_action, self.fra_action)

        # frame 2
        self.fra_2 = QFrame(self.fra_main)
        fra_2_layer = QGridLayout(self.fra_2)
        fra_2_layer.setContentsMargins(5, 0, 10, 0)

        self.chk_use_selected = QCheckBox("Go to selected features")
        fra_2_layer.addWidget(self.chk_use_selected, 0, 0)

        self.chk_start_selected = QCheckBox("Start from selected feature")
        fra_2_layer.addWidget(self.chk_start_selected, 0, 1)

        self.chk_select = QCheckBox("Select feature")
        fra_2_layer.addWidget(self.chk_select, 1, 0)

        self.btn_copy = QPushButton("Copy Feature")
        self.btn_copy.setMaximumWidth(79)
        self.btn_copy.setLayoutDirection(Qt.RightToLeft)
        fra_2_layer.addWidget(self.btn_copy, 1, 1)

        # frame 3
        self.fra_3 = QFrame(self.fra_main)
        fra_3_layer = QGridLayout(self.fra_3)

        self.lbl_feat_count1 = QLabel("Feature no.")
        fra_3_layer.addWidget(self.lbl_feat_count1, 0, 0)

        self.lbl_feat_count = QLabel("- of -")
        fra_3_layer.addWidget(self.lbl_feat_count, 0, 1)

        self.lbl_current = QLabel("Value:")
        fra_3_layer.addWidget(self.lbl_current, 1, 0)

        self.txt_value = QLineEdit()
        self.txt_value.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.txt_value.setReadOnly(True)
        self.txt_value.setMaximumHeight(21)
        fra_3_layer.addWidget(self.txt_value, 1, 1)

        self.btn_show_osm = QPushButton("Show on OSM")
        fra_3_layer.addWidget(self.btn_show_osm, 1, 2)

        # frame 4
        self.fra_4 = QFrame(self.fra_main)
        fra_4_layer = QGridLayout(self.fra_4)

        self.lbl_state = QLabel("State:")
        fra_4_layer.addWidget(self.lbl_state, 0, 0)

        self.fra_state = QFrame(self.fra_4)
        fra_state_layer = QGridLayout(self.fra_state)
        fra_state_layer.setContentsMargins(50, 0, 50, 0)

        self.rad_state_undefined = QRadioButton("undefined")
        self.rad_state_undefined.setChecked(True)
        self.rad_state_unknown = QRadioButton("unknown")
        self.rad_state_fixed = QRadioButton("fixed")
        self.rad_state_already_fixed = QRadioButton("already fixed")
        self.rad_state_no_issue = QRadioButton("no issue")

        self.group_rad_state = QButtonGroup(self.fra_4)
        self.group_rad_state.addButton(self.rad_state_undefined)
        self.group_rad_state.addButton(self.rad_state_unknown)
        self.group_rad_state.addButton(self.rad_state_fixed)
        self.group_rad_state.addButton(self.rad_state_already_fixed)
        self.group_rad_state.addButton(self.rad_state_no_issue)

        fra_state_layer.addWidget(self.rad_state_fixed, 0, 0)
        fra_state_layer.addWidget(self.rad_state_already_fixed, 0, 1)
        fra_state_layer.addWidget(self.rad_state_no_issue, 1, 0)
        fra_state_layer.addWidget(self.rad_state_unknown, 1, 1)
        fra_state_layer.addWidget(self.rad_state_undefined, 2, 0)

        fra_4_layer.addWidget(self.fra_state, 1, 0)

        # frame 5
        self.fra_5 = QFrame(self.fra_main)
        fra_5_layer = QHBoxLayout(self.fra_5)
        fra_5_layer.setSpacing(0)

        self.btn_first_feature = QPushButton("<<")
        self.btn_first_feature.setMaximumWidth(45)
        fra_5_layer.addWidget(self.btn_first_feature)

        self.btn_prev = QPushButton("<")
        fra_5_layer.addWidget(self.btn_prev)

        self.btn_next = QPushButton(">")
        fra_5_layer.addWidget(self.btn_next)

        self.btn_last_feature = QPushButton(">>")
        self.btn_last_feature.setMaximumWidth(45)
        fra_5_layer.addWidget(self.btn_last_feature)

        # Add everything to main frame
        fra_main_layer.addWidget(self.fra_1)
        fra_main_layer.addWidget(self.fra_2)
        fra_main_layer.addWidget(self.fra_3)
        fra_main_layer.addWidget(self.fra_4)
        fra_main_layer.addWidget(self.fra_5)

        # save states
        self.states = {
            self.rad_state_undefined.text(): self.rad_state_undefined,
            self.rad_state_unknown.text(): self.rad_state_unknown,
            self.rad_state_fixed.text(): self.rad_state_fixed,
            self.rad_state_already_fixed.text(): self.rad_state_already_fixed,
            self.rad_state_no_issue.text(): self.rad_state_no_issue,
        }

        self.setup()

    def setup(self):
        self.setWindowTitle(Go2NextFeatureDockWidget.plugin_name)
        # self.chk_select.setEnabled(False)

        QgsProject.instance().layerWasAdded.connect(self.update_layers_combos)
        QgsProject.instance().layerRemoved.connect(self.update_layers_combos)

        self.chk_state.toggled.connect(self.chk_activate_deactivate_state)
        self.chk_use_selected.toggled.connect(self.chk_use_selected_clicked)
        self.chk_start_selected.toggled.connect(self.chk_start_selected_clicked)

        self.cbo_layer.activated.connect(self.cbo_layer_activated)
        self.cbo_order_by.activated.connect(self.cbo_order_by_activated)
        self.cbo_state.activated.connect(self.activate_state)

        self.btn_prev.pressed.connect(lambda: self.next_feat(ButtonType.PREVIOUS))
        self.btn_copy.pressed.connect(self.btn_copy_pressed)
        self.btn_show_osm.pressed.connect(self.btn_show_osm_pressed)
        self.btn_first_feature.pressed.connect(lambda: self.next_feat(ButtonType.FIRST))
        self.btn_last_feature.pressed.connect(lambda: self.next_feat(ButtonType.LAST))
        self.btn_next.pressed.connect(lambda: self.next_feat(ButtonType.NEXT))

        self.update_layers_combos()

        # Turn off controls
        self.toggle_controls(False)

        self.chk_activate_deactivate_state()

        # Tooltips
        self.chk_use_selected.setToolTip(
            "Go to selected features only"
        )

        self.btn_copy.setToolTip(
            "Copy feature text as attribute-value pairs (Ctrl-Shift-8)"
        )

        self.btn_show_osm.setToolTip(
            "Show a feature on OSM by its coordinates"
        )

        self.btn_first_feature.setToolTip(
            "First feature"
        )

        self.btn_last_feature.setToolTip(
            "Last feature"
        )

        self.btn_prev.setToolTip(
            "Previous feature"
        )

        self.btn_next.setToolTip(
            "Next feature (F8)"
        )

        # Shortcuts
        self.btn_next.setShortcut("F8")
        self.btn_copy.setShortcut(QtGui.QKeySequence("Ctrl+Shift+8"))

        # radiobutton functionality
        self.rad_state_undefined.toggled.connect(
            lambda: self.rad_btn_state(self.rad_state_undefined)
        )
        self.rad_state_unknown.toggled.connect(
            lambda: self.rad_btn_state(self.rad_state_unknown)
        )
        self.rad_state_fixed.toggled.connect(
            lambda: self.rad_btn_state(self.rad_state_fixed)
        )
        self.rad_state_already_fixed.toggled.connect(
            lambda: self.rad_btn_state(self.rad_state_already_fixed)
        )
        self.rad_state_no_issue.toggled.connect(
            lambda: self.rad_btn_state(self.rad_state_no_issue)
        )

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    def update_layers_combos(self):
        prev_layer_id = None
        if self.cbo_layer.currentIndex() is not None:
            prev_layer_id = self.cbo_layer.itemData(self.cbo_layer.currentIndex())

        # Find layers IDs and names
        layer_ids = QgsProject.instance().mapLayers()
        names_ids_dict = {}
        for layer_id in layer_ids:
            layer = self.get_layer_from_id(layer_id)
            if layer is not None and layer.type() == 0:
                names_ids_dict[layer_id] = layer.name()

        # Fill combo
        self.cbo_layer.clear()
        self.cbo_layer.addItem("", None)

        sorted_ids_names = sorted(list(names_ids_dict.items()), key=operator.itemgetter(1))

        for name_id in sorted_ids_names:
            self.cbo_layer.addItem(name_id[1], name_id[0])

        if prev_layer_id is not None:
            self.layer_id = self.set_layercombo_index(self.cbo_layer, prev_layer_id)

        else:
            self.toggle_navigation_btn(self.btn_prev, self.layer_id is not None)
            self.toggle_navigation_btn(self.btn_next, self.layer_id is not None)
            self.cbo_layer_activated()

            self.show_feature_count()

        # Disables buttons if there is no layer selected
        if self.layer_id is None:
            self.toggle_controls(False)

    def reset_navigation_buttons(self):
        self.toggle_navigation_btn(self.btn_prev, False)
        self.toggle_navigation_btn(self.btn_next, True)
        self.toggle_navigation_btn(self.btn_first_feature, False)
        self.toggle_navigation_btn(self.btn_last_feature, True)
        self.btn_show_osm.setEnabled(False)

    def chk_start_selected_clicked(self):
        if self.chk_start_selected.isChecked():
            if len(self.selected_features) != 1:
                self.warn("Please select one feature.")
                return

            self.feat_index = None
            self.selected_feat_id = None

            self.reset_navigation_buttons()

    def chk_use_selected_clicked(self):
        if self.chk_use_selected.isChecked() and len(self.selected_features) < 1:
            self.chk_use_selected.setChecked(False)
            self.warn("Please select at least one feature.")
            return

        self.feat_index = None
        self.selected_feat_id = None
        self.chk_start_selected.setEnabled(not self.chk_use_selected.isChecked())
        self.chk_select.setEnabled(not self.chk_use_selected.isChecked())

        self.reset_navigation_buttons()

    def cbo_layer_activated(self):
        self.layer_id = self.cbo_layer.itemData(self.cbo_layer.currentIndex())

        self.chk_use_selected.setChecked(False)
        if self.layer_id is not None:

            self.layer = self.get_layer_from_id(self.layer_id)

            # Register a listener for selection changed
            self.layer.selectionChanged.connect(self.selection_changed)

            fields = self.layer.fields()
            self.cbo_order_by.clear()
            self.cbo_state.clear()
            self.cbo_state.addItem("", None)
            for count, field in sorted(enumerate(fields), key=lambda field: field[1].name().upper()):
                self.attribs_dict[field.name()] = field
                self.cbo_order_by.addItem(field.name(), field)
                # 10 = FieldType String / primaryKeyAttributes returns id
                if field.type() == 10 and count not in self.layer.primaryKeyAttributes():
                    self.cbo_state.addItem(field.name(), field)
            self.cbo_order_by_activated()
            self.layer.editingStopped.connect(self.editing_ended)
            if len(list(self.layer.getFeatures())) > 0:
                self.toggle_controls(True)
            self.btn_show_osm.setEnabled(False)
            self.reload_selected_features()
        else:
            self.layer = None
            self.feats_list.clear()
            self.attrib = None
            self.attribs_dict.clear()
            self.toggle_controls(False)
            self.show_feature_count()

    def reload_selected_features(self):
        self.selected_features = self.sort_list(self.layer.selectedFeatures())

    def selection_changed(self):
        self.reload_selected_features()
        self.chk_use_selected.setChecked(False)
        self.selected_feat_index = None

    def activate_state(self):
        result = self.should_state_be_active()
        self.rad_state_unknown.setEnabled(result)
        self.rad_state_fixed.setEnabled(result)
        self.rad_state_already_fixed.setEnabled(result)
        self.rad_state_no_issue.setEnabled(result)
        self.rad_state_undefined.setEnabled(result)
        return result

    def should_state_be_active(self):
        attrib = self.cbo_state.itemData(self.cbo_state.currentIndex())
        result = True
        if attrib is None or not self.cbo_state.isEnabled():
            result = False
        return result

    def cbo_order_by_activated(self):
        self.feats_list = self.sort_list(self.layer.getFeatures())
        self.reload_selected_features
        self.feat_index = None
        self.selected_feat_index = None
        self.reset_navigation_buttons()
        self.show_feature_count()

    def sort_list(self, qgis_feature_list):
        key_attribute_name = self.cbo_order_by.currentText()
        return sorted(qgis_feature_list, key=lambda x: x.attribute(key_attribute_name))

    def editing_ended(self):
        self.cbo_state.setEnabled(False)
        self.cbo_state.setCurrentIndex(0)
        self.chk_state.setChecked(False)
        for rad_state in self.states.values():
            rad_state.setEnabled(False)
        self.set_state("undefined")

    def toggle_controls(self, should_activate):
        self.chk_use_selected.setEnabled(should_activate)
        self.chk_start_selected.setEnabled(should_activate)
        self.cbo_order_by.setEnabled(should_activate)
        self.chk_state.setEnabled(should_activate)
        self.chk_select.setEnabled(should_activate)
        self.rad_action_zoom.setEnabled(should_activate)
        self.rad_action_pan.setEnabled(should_activate)
        self.btn_copy.setEnabled(should_activate)
        self.btn_show_osm.setEnabled(should_activate)

        self.toggle_navigation_btn(self.btn_prev, False)
        self.toggle_navigation_btn(self.btn_first_feature, False)
        self.chk_state.setChecked(False)

        if len(self.feats_list) == 0:
            should_activate = False

        self.toggle_navigation_btn(self.btn_next, should_activate)
        self.toggle_navigation_btn(self.btn_last_feature, should_activate)

    def set_state(self, state_attribute):
        if not self.states[state_attribute].isChecked():
            self.changed_state_with_show = True
            self.states[state_attribute].setChecked(True)

    def edit_state(self, selected_state):
        if self.feat_index is None:
            return
        if self.control_is_editable():
            state_attribute_id, _ = self.state_id_attribute()
            attrs = {state_attribute_id: selected_state}
            self.layer.changeAttributeValues(self.feats_list[self.feat_index].id(), attrs)
            self.feats_list[self.feat_index][state_attribute_id] = selected_state

    def state_id_attribute(self):
        attrib = self.cbo_state.itemData(self.cbo_state.currentIndex())
        state_attribute_id = first_occurrence_index(attrib.name(), (field.name() for field in self.layer.fields()))
        state_attribute = str(self.feats_list[self.feat_index].attributes()[state_attribute_id])
        return state_attribute_id, state_attribute

    def chk_activate_deactivate_state(self):
        if self.control_is_editable():
            self.cbo_state.setEnabled(self.chk_state.isChecked())
            self.activate_state()

    def control_is_editable(self):
        if self.layer and not self.layer.isEditable() and self.chk_state.isChecked():
            self.warn("Please set the selected layer in edit mode first.")
            self.chk_state.setChecked(False)
            return False
        return True

    def rad_btn_state(self, button):
        if button.isChecked():
            if self.changed_state_with_show:
                self.changed_state_with_show = False
            else:
                if button.text() == "undefined":
                    self.edit_state(NULL)
                else:
                    self.edit_state(button.text())

    def btn_copy_pressed(self):
        selected_feature = self.layer.selectedFeatures()
        if len(selected_feature) != 1:
            self.warn("Please select one feature from the selected Layer.")
            return

        clipboard = QApplication.clipboard()
        clipboard.setText(self.osm_field_attribute_string(selected_feature))

    def btn_show_osm_pressed(self):
        geom = self.current_feature().geometry()
        if geom is None or geom.isNull():
            self.warn("No feature/coordinates.")
        else:
            crs_src = self.layer.crs()
            crs_dest = QgsCoordinateReferenceSystem(4326)
            xform = QgsCoordinateTransform(crs_src, crs_dest, QgsProject.instance())
            point_coords = geom.centroid().asPoint()

            pt1 = xform.transform(QgsPointXY(point_coords[0], point_coords[1]))

            lat = self.cap_coordinates(pt1[1])
            lon = self.cap_coordinates(pt1[0])
            webbrowser.open(f"https://www.openstreetmap.org/?mlat={lat}&mlon={lon}#map=18/{lat}/{lon}")

    def toggle_navigation_btn(self, btn, should_activate):
        font = QtGui.QFont()
        font.setBold(should_activate)
        btn.setFont(font)

        btn.setEnabled(should_activate)

    def show_feature_count(self):
        if not self.layer_id or self.feat_index is None:
            self.txt_value.setText("-")
            self.lbl_feat_count.setText("- of %d features" % (len(self.feats_list)))
        else:
            self.lbl_feat_count.setText(
                "%d of %d" % ((self.feat_index + 1), len(self.feats_list))
            )

    def next_feat(self, button_type):
        if self.chk_start_selected.isChecked():
            try:
                self.start_from_selected()
            except ValueError:
                return

        elif self.chk_use_selected.isChecked():
            self.go_to_next_selected_feature(button_type)

        # Go to next item
        else:
            self.feat_index = self.change_index(button_type, self.feat_index, self.feats_list)

        # Update Navigation
        if self.should_state_be_active():
            if self.feat_index is None:
                return
            _, state_attribute = self.state_id_attribute()
            if state_attribute not in self.states.keys():
                self.set_state("undefined")
            else:
                self.set_state(state_attribute)

        self.focus_on_feature()
        self.show_feature_count()

    def start_from_selected(self):
        if len(self.selected_features) != 1:
            self.warn("Please select exactly one feature.")
            raise ValueError()

        reference_id = self.selected_features[0].id()
        self.feat_index = first_occurrence_index(reference_id, (feat.id() for feat in self.feats_list))
        self.chk_start_selected.setChecked(False)
        self.enable_disable_navigation(self.feat_index, self.feats_list)

    def go_to_next_selected_feature(self, button_type):
        self.selected_feat_index = self.change_index(button_type, self.selected_feat_index, self.selected_features)
        reference_id = self.selected_features[self.selected_feat_index].id()
        self.feat_index = first_occurrence_index(reference_id, (feat.id() for feat in self.feats_list))

    def change_index(self, button_type, index, generic_list):
        if index is None:
            index = 0
        elif button_type == ButtonType.NEXT:
            index += 1
        elif button_type == ButtonType.PREVIOUS:
            index += -1
        elif button_type == ButtonType.FIRST:
            index = 0
        elif button_type == ButtonType.LAST:
            index = len(generic_list) - 1
        else:
            self.warn("selected_feat_index is invalid")

        self.enable_disable_navigation(index, generic_list)
        return index

    def enable_disable_navigation(self, index, generic_list):

        has_next = index < len(generic_list) - 1
        self.toggle_navigation_btn(self.btn_next, has_next)
        self.toggle_navigation_btn(self.btn_last_feature, has_next)

        has_previous = index > 0
        self.toggle_navigation_btn(self.btn_prev, has_previous)
        self.toggle_navigation_btn(self.btn_first_feature, has_previous)

    def focus_on_feature(self):
        self.btn_show_osm.setEnabled(True)
        geom = self.current_feature().geometry()
        if geom is None or geom.isNull():
            self.warn(
                "The geometry of the feature is null: can neither zoom nor pan to it."
            )
            self.txt_value.setText("-")
        else:
            self.move_to_geometry(geom)
            self.iface.mapCanvas().refresh()

            self.txt_value.setText(str(self.get_attribute_value_from_current_feature()))

        if self.chk_select.isChecked():
            self.layer.selectByIds([self.current_feature().id()])

    def get_attribute_value_from_current_feature(self):
        return self.current_feature().attribute(self.cbo_order_by.currentText())

    def move_to_geometry(self, geom):
        renderer = self.iface.mapCanvas().mapSettings()

        if self.rad_action_pan.isChecked():
            self.iface.mapCanvas().setCenter(renderer.layerToMapCoordinates(self.layer, geom.centroid().asPoint()))
        elif self.rad_action_zoom.isChecked():
            max_length = max(geom.boundingBox().width(), geom.boundingBox().height())

            self.iface.mapCanvas().setExtent(
                renderer.layerToMapCoordinates(self.layer, geom.boundingBox().buffered(max_length * 0.4))
            )

    def set_layercombo_index(self, combo, layer_id):

        index = combo.findData(layer_id)
        if index >= 0:
            combo.setCurrentIndex(index)
            return self.get_layer_from_id(
                self.get_combo_current_data(combo)
            )
        else:
            if combo.count() > 0:
                combo.setCurrentIndex(0)
                return combo.itemData(0)
            else:
                return None

    @staticmethod
    def get_combo_current_data(combo):
        index = combo.currentIndex()
        return combo.itemData(index)

    def set_cursor(self, cursor_shape):
        cursor = QtGui.QCursor()
        cursor.setShape(cursor_shape)
        self.iface.mapCanvas().setCursor(cursor)

    @staticmethod
    def osm_field_attribute_string(selected_feature):
        fields = [str(field.name()) for field in selected_feature[0].fields()]
        attributes = [str(attribute) for attribute in selected_feature[0].attributes()]

        for index, field in enumerate(fields):
            if field[0] == '@':
                fields.pop(index)
                attributes.pop(index)

        for index, attribute in enumerate(attributes):
            if attribute.lower() == "null":
                fields.pop(index)
                attributes.pop(index)

        return "\n".join(
            "{!s}={!s}".format(key, val)
            for (key, val) in dict(zip(fields, attributes)).items()
        )

    def current_feature(self):
        return self.feats_list[self.feat_index]

    def get_layer_from_id(self, lay_id):
        return QgsProject.instance().mapLayer(lay_id)

    def warn(self, message):
        self.iface.messageBar().pushMessage(
            Go2NextFeatureDockWidget.plugin_name, message, Qgis.Warning,
        )

    def cap_coordinates(self, coord):
        str_coord = str(coord)
        for i, char in enumerate(str_coord):
            if char == ".":
                dot_index = i
                break
        else:
            return str_coord
        return str_coord[:dot_index + 6]
