# This file contains metadata for the plugin. 

# Mandatory items:

[general]
name=Go2NextFeaturePlus
qgisMinimumVersion=2.99
qgisMaximumVersion=3.99
description=Allows jumping from a feature to another following an attribute order
version=2.10
author=Christopher Hilfing, Hanna Wildermuth, Stefan Keller, Alberto De Luca (for Tabacco Editrice)
email=geometalab@gmail.com

about=This plugin allows to select a layer and jump from a feature to the next. It's possible to select an attribute of the layer (e.g. name) which defines the ordering. Besides the Pan function there's a Pan and Zoom function for polygons and lines and other options. In addition to the mentioned functionality it is also possible to set and persist a state ("fixed", "already fixed", "no issue", "unknown" or "undefined") of each feature. For this the layer needs to be updatable (which excludes e.g. formats like CSV or GeoJSON which need to be converted e.g. to GeoPackage).

tracker=https://gitlab.com/geometalab/Go2NextFeaturePlus/-/issues
repository=https://gitlab.com/geometalab/Go2NextFeaturePlus
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
changelog=
2.00: Migrated to QGIS3.
2.10: Added functionality to copy attributes in key=value style and introduce states.

# Tags are comma separated with spaces allowed
tags=python, osm, openstreetmap, vector, reconciliation

homepage=https://gitlab.com/geometalab/Go2NextFeaturePlus/-/blob/master/README.md
category=Plugins, Vector
icon=icon.png
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Comma separated list of plugins to be installed or upgraded (since QGIS 3.8).
# plugin_dependencies=